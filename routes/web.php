<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Movie list as homepage
Route::get('/', function(){
	return redirect('movies');
});

// Movie list
Route::resource('/movies', 'MovieController');
