@extends("main")

@section("content")


<!-- <link rel="stylesheet" type="text/css" href="main.css"> -->

	<div class="container">
	<h3>Movies</h3>
		<div class="row">
			@foreach ($movies as $movie)
				<div id="movies" class="col-md-3 col-sm-12 col-xs-12">
					<div class="movie-images">
						<h3>{{ $movie['title'] }}</h3>
						<img src="{{$movie['image']}}" width="200">
						<div class="movie-images-body">
							<h4>{{ $movie['year'] }}</h4>
							<h5>{{ $movie['duration'] }} mins</h5>
							<h5>{{ $movie['description'] }}</h5>
						</div>
					</div>
				</div>
			@endforeach
		</div>	
	</div>

@endsection