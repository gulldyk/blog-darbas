@extends("main")

@section("content")


<!-- <link rel="stylesheet" type="text/css" href="main.css"> -->
<article>
    <div class="container form-apply">
        <div class="row">
            <div class="col-md-12 ">
            	<!-- <h3></h3> -->
                <form class="form-horizontal" action="{{ route('movies.store') }}" method="POST">
                			{{ csrf_field() }}
					<fieldset>
						<legend>{{ isset($movie) ? 'Edit movie' : 'Add new movie' }}</legend>
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Item title</label>  
								<div class="col-md-4">
								@if(isset($movie))
									<input value="{{ $movie['title'] }}" name="title" type="text" placeholder="" class="form-control input-md">
								@else
									<input name="title" type="text" placeholder="" class="form-control input-md">
								@endif		
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="year">Year</label> 
								<div class="col-md-4">
								@if(isset($movie))
									<input value="{{ $movie['year'] }}" name="year" type="text" placeholder="" class="form-control input-md">
								@else	
									<input name="year" type="text" placeholder="" class="form-control input-md">
								@endif	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="duration">Duration min.</label> 
								<div class="col-md-4">
								@if(isset($movie))
									<input value="{{ $movie['duration'] }}" name="duration" type="text" placeholder="" class="form-control input-md">
								@else	
									<input name="duration" type="text" placeholder="" class="form-control input-md">
								@endif	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="description">Description</label>
								<div class="col-md-4">
								@if(isset($movie))                     
									<textarea class="form-control" rows="10" name="description" placeholder="{{ $movie['description'] }}"></textarea>
								@else	
									<textarea class="form-control" name="description"></textarea>
								@endif	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="image">Image URL</label>
								<div class="col-md-4">
								@if(isset($movie))                     
									<textarea class="form-control" rows="5" name="image" placeholder="{{ $movie['image'] }}"></textarea>
								@else	
									<textarea class="form-control" name="image"></textarea>
								@endif	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="singlebutton">Add new item to my shop</label>
								<div class="col-md-4">
									<input type="submit" name="save" class="btn btn-inverse"></input>
								</div>
							</div>
					</fieldset>
				</form>
            </div>
        </div>
    </div> <!-- container main -->   
</article> 
@endsection