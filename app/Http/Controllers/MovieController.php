<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = [

        [

        "title" => "The Secret Life of Pets",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "image" => "http://cdn.movieweb.com/img.teasers.posters/FIqObXLW646Ctz_377_a.jpg",
        "year" => 2001,
        "duration" => 120


        ],

        [

        "title" => "The Hateful Eight",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "image" => "http://cdn.movieweb.com/img.teasers.posters/FIGhP06utvaYJM_389_a.jpg",
        "year" => 2002,
        "duration" => 120


        ],

        [

        "title" => "Ghostbusters",
        "description" => "30 years after Ghostbusters took the world by storm, the beloved franchise makes its long-awaited return. Director Paul Feig brings his fresh take to the supernatural comedy, joined by some of the funniest actors working today.",
        "image" => "http://cdn.movieweb.com/img.teasers.posters/FInCjpptml82pw_248_a.jpg",
        "year" => 2002,
        "duration" => 120


        ],

        [

        "title" => "Suicide Squad",
        "description" => "It feels good to be bad… Assemble a team of the world’s most dangerous, incarcerated Super Villains, provide them with the most powerful arsenal at the government’s disposal, and send them off on a mission to defeat an enigmatic, insuperable entity. U.S. intelligence officer Amanda Waller has determined only a secretly convened group of disparate, despicable individuals with next to nothing to lose will do. However, once they realize they weren’t picked to succeed but chosen for their patent culpability when they inevitably fail, will the Suicide Squad resolve to die trying, or decide it’s every man for himself?",
        "image" => "http://cdn.movieweb.com/img.teasers.posters/FIs3ZdMwCtOVww_370_a.jpg",
        "year" => 2016,
        "duration" => 90


        ]

    ];

    return view('movies.index', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->title);
        // dd($request->toArray());

        return redirect()->route('movies.index')->with('status', 'Movie added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = 

        [

        "title" => "The Secret Life of Pets",
        "description" => "For one bustling Manhattan apartment building, the real day starts after the folks on two legs leave for work and school. That’s when the pets of every stripe, fur, and feather begin their own nine-to-five routine: hanging out with each other, trading humiliating stories about their owners or auditioning adorable looks to get better snacks. The buildings top dog, Max (voiced by Louis C.K.) a quick witted terrier rescue who’s convinced he sits at the center of his owner’s universe, finds his pampered life rocked when she brings home Duke (Eric Stonestreet)",
        "image" => "http://cdn.movieweb.com/img.teasers.posters/FIqObXLW646Ctz_377_a.jpg",
        "year" => 2001,
        "duration" => 120


        ];

    
    return view('movies.create', compact('movie'));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
